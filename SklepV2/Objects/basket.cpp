#include "basket.h"
#include <iostream>
#include "Managers/logger.h"
std::set<ShopPosition*> Basket::basketPositions;

Basket & Basket::getBasket(){
    static Basket instance;
    return instance;
}
ShopPosition * Basket::getProductByName(std::string productName){
    for (auto it: basketPositions){
        if (productName == it->product->name){
            return it;
        }
    }
    return nullptr;
}
void Basket::changeProductCount(ShopPosition *pos, int count){
    if (count==0){
        //
        return;
    }
    ShopPosition * productPos = getProductByName(pos->product->name);
    if (productPos == nullptr){
        Logger::getLogger().Log("Can't find specified Product in class: "+getElementType(),1);
        return;
    }
    productPos->count += count;
}
void Basket::addToBasket(ShopPosition *pos){
    ShopPosition * productPos = getProductByName(pos->product->name);
    if (productPos == nullptr){
        basketPositions.insert(pos);
        return;
    }

    changeProductCount(pos,pos->count);
}
void Basket::removeFromBasket(std::string productName, int count){
    if (count<=0){
        Logger::getLogger().Log("Wrong count specified in class: "+getElementType(),1);
    }
    for (auto it:basketPositions){
        if (productName == it->product->name){
            if (it->count == count){
                basketPositions.erase(basketPositions.find(it));
            }
            if (it->count < count){
                // ERROR
                return;
            }
            it->count -= count;
            return;
        }
    }
    Logger::getLogger().Log("Can't find specified Product in class: "+getElementType(),1);
}
void Basket::showBasket(){
    std::cout<<"PRODUCT:\tCOUNT:\tNETTO:\tBRUTTO:\tSNETTO:\tSBRUTTO:\tSUM TAX:\t\n";
    for (auto it : basketPositions){
        std::cout<<it->product->name<<"\t"<<
                   it->count<<"\t"<<
                   it->product->priceNetto<<"\t"<<
                   it->product->priceBrutto<<"\t"<<
                   it->count*it->product->priceNetto<<"\t"<<
                   it->count*it->product->priceBrutto<<"\t"<<
                   it->count*it->product->tax<<"\t\n";
    }
}
std::string Basket::getElementType(){
    return "Basket";
}
