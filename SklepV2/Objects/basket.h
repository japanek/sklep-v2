#ifndef BASKET_H
#define BASKET_H

#include <set>
#include "Interfaces/programclass.h"

#include "Objects/shopposition.h"

class Basket: public ProgramClass
{
public:

    static std::set<ShopPosition*> basketPositions;
    static Basket & getBasket();
    static ShopPosition *getProductByName(std::string productName);
    static void addToBasket(ShopPosition * pos);
    static void changeProductCount (ShopPosition * pos, int count);
    static void removeFromBasket(std::string productName, int count);

    static void showBasket();
    static std::string getElementType();
private:
    /*** DISABLE COPY OPERATOR ***/
    Basket (Basket const &) = delete;
    Basket(Basket &) = delete;
    /*****************************/
    Basket(){};
};

#endif // BASKET_H
