#include "stock.h"

#include "Objects/productfactory.h"
#include "Managers/logger.h"

#include <iostream>
#include <fstream>
#include <string>
std::set<ShopPosition*> Stock::stock;

void Stock::addToStock(ShopPosition * position){
    for (auto it:stock){
        if (position->product->name == it->product->name){
            it->count += position->count;
            return;
        }
    }
    stock.insert(position);
}
void Stock::removeFromStock(std::string productName, int count){
    if (count<=0){
        Logger::getLogger().Log("Wrong count specified in class: "+getElementType(),1);
    }
    for (auto it:stock){
        if (productName == it->product->name){
            if (it->count == count){
                stock.erase(stock.find(it));
            }
            if (it->count < count){
                Logger::getLogger().Log("Wrong count specified in class: "+getElementType(),1);
                return;
            }
            it->count -= count;
            return;
        }
    }
    Logger::getLogger().Log("Can't find specified Product in class: "+getElementType(),1);
}
void Stock::readStockStateFromFile(std::string fileName){
    std::ifstream input(fileName);
    if (input){
        std::string word;
        while (input>>word){
            std::string productName = word;
            input >> word;
            int count = std::atoi(word.c_str());
            stock.insert(new ShopPosition(ProductFactory::createProduct(productName),count));
        }
        input.close();
        Logger::getLogger().Log("Reading from file"+fileName+" COMPLETED "+" in class: "+getElementType(),2);
        return;
    }
    Logger::getLogger().Log("Can't open file "+fileName+" in class: "+getElementType(),1);
}
void Stock::saveStockStateToFile(std::string fileName){
    std::ofstream output(fileName);
    if (output){
        for (auto it:stock){
            output<<it->product->name<<" "<<it->count<<"\n";
        }
        output.close();
        Logger::getLogger().Log("Saving to file"+fileName+" COMPLETED "+" in class: "+getElementType(),2);
        return;
    }
    Logger::getLogger().Log("Can't open file "+fileName+" in class: "+getElementType(),1);
}
void Stock::getDeliveryFromFile(std::string fileName){
    std::ifstream input(fileName);
    if (input){
        std::string word;
        while (input>>word){
            std::string productName = word;
            input >> word;
            int count = std::atoi(word.c_str());
            addToStock(new ShopPosition(ProductFactory::createProduct(productName),count));
        }
        input.close();
        return;
    }
    Logger::getLogger().Log("Can't open file "+fileName+" in class: "+getElementType(),1);

}
ShopPosition * Stock::getPositionByName(std::string productName){
    for (auto it : stock){
        if (productName == it->product->name){
            return it;
        }
    }
    Logger::getLogger().Log("Can't find specified Product in class: "+getElementType(),1);
    return nullptr;
}
Stock & Stock::getStock(){
    static Stock instance;
    return instance;
}
void Stock::showStock(){
    std::cout<<"PRODUCT:\tCOUNT:\tNETTO:\tBRUTTO:\tSNETTO:\tSBRUTTO:\tSUM TAX:\t\n";
    for (auto it : stock){
        std::cout<<it->product->name<<"\t"<<
                   it->count<<"\t"<<
                   it->product->priceNetto<<"\t"<<
                   it->product->priceBrutto<<"\t"<<
                   it->count*it->product->priceNetto<<"\t"<<
                   it->count*it->product->priceBrutto<<"\t"<<
                   it->count*it->product->tax<<"\t\n";
    }
}

std::string Stock::getElementType(){
    return "Stock";
}
