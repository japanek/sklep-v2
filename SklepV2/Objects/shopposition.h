#ifndef SHOPPOSITION_H
#define SHOPPOSITION_H

#include "Interfaces/product.h"

class ShopPosition
{
public:
    ShopPosition(Product * product, int count);
    Product * product;
    int count;
};

#endif // SHOPPOSITION_H
