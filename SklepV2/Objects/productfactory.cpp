#include "productfactory.h"
#include "Products/milk.h"
#include "Products/bread.h"
#include "Products/sugar.h"


Product * ProductFactory::createProduct(std::string productType){
    Product * p;
    if (productType == "Milk"){
        p = new Milk(); }
    else if (productType == "Bread"){
        p = new Bread ();
    }
    else if (productType == "Sugar"){
        p = new Sugar();
    }
    return p;
}
std::string ProductFactory::getElementType(){
    return "ProductFactory";
}
