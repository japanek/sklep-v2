#ifndef STOCK_H
#define STOCK_H

#include <set>

#include "Objects/shopposition.h"
#include "Interfaces/programclass.h"

class Stock: public ProgramClass
{
public:

    static Stock & getStock();
    static std::set<ShopPosition*> stock;
    ShopPosition * getPositionByName(std::string productName);
    static void addToStock(ShopPosition * position);
    static void removeFromStock(std::string productName, int count);

    /********* FILE OPPERATIONS ********/
    static void saveStockStateToFile(std::string fileName);
    static void readStockStateFromFile(std::string fileName);
    static void getDeliveryFromFile(std::string fileName);
    //********* FILE OPPERATIONS ********/

    static void showStock();

    static std::string getElementType();
private:
    /*** DISABLE COPY OPERATOR ***/
    Stock (Stock const &) = delete;
    Stock (Stock &) = delete;
    /*****************************/
    Stock(){};
};

#endif // STOCK_H
