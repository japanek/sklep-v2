#ifndef PRODUCTFACTORY_H
#define PRODUCTFACTORY_H

#include "Interfaces/product.h"

#include "Interfaces/programclass.h"

class ProductFactory : public ProgramClass
{
public:
    static Product * createProduct(std::string productType);
    std::string getElementType();
private:
    ProductFactory(){};
};

#endif // PRODUCTFACTORY_H
