#ifndef ORDERMANAGER_H
#define ORDERMANAGER_H

#include "Objects/basket.h"
#include "Objects/stock.h"
#include "Interfaces/programclass.h"

class OrderManager: public ProgramClass
{
public:
    OrderManager();
    void init(Basket * basket, Stock * stock);
    void addToBasket(std::string productName, int count);
    void removeFromBasket(std::string productName, int count);
    void order();
    void cancelOrder();
    std::string getElementType();
private:
    Basket * basket;
    Stock * stock;
};

#endif // ORDERMANAGER_H
