#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <fstream>

class Logger
{
public:
    static Logger & getLogger();
    void Log (const std::string& msg, int type =0);
    Logger & operator<<(const std::string msg);
private:

    static std::string logFileName;
    static std::ofstream logFile;

    Logger(){};
    Logger(Logger const &) = delete;
    Logger (Logger &) = delete;

};

#endif // LOGGER_H
