#include "ordermanager.h"
#include "Managers/logger.h"

OrderManager::OrderManager(){

}
void OrderManager::addToBasket(std::string productName, int count){
    if (count<=0){
        Logger::getLogger().Log("Wrong count specified in class"+getElementType(),1);
        return;
    }
    ShopPosition * shopPos = stock->getPositionByName(productName);
    if (shopPos == nullptr){
        Logger::getLogger().Log("Can't find specified Product in class"+getElementType(),1);
        return;
    }
    if (stock->getPositionByName(productName)->count < count){
        Logger::getLogger().Log("Wrong count specified in class: "+getElementType(),1);
        return;
    }
    Basket::getBasket().addToBasket(new ShopPosition(shopPos->product,count));
}
void OrderManager::init(Basket *basket, Stock *stock){
    this->basket = basket;
    this->stock = stock;
}
void OrderManager::removeFromBasket(std::string productName, int count){
    if (count<=0){
        Logger::getLogger().Log("Wrong count specified in class: "+getElementType(),1);
        return;
    }
    ShopPosition * shopPos = basket->getProductByName(productName);
    if (shopPos == nullptr){
        Logger::getLogger().Log("Can't find specified Product in class: "+getElementType(),1);
        return;
    }
    if (shopPos->count < count){
        Logger::getLogger().Log("Wrong count specified in class: "+getElementType(),1);
        return;
    }
    stock->addToStock(new ShopPosition(shopPos->product,count));
}
void OrderManager::order(){
    for (auto it : basket->basketPositions){
        Stock::getStock().removeFromStock(it->product->name,it->count);
    }
    basket->basketPositions.clear();
}
void OrderManager::cancelOrder(){
    basket->basketPositions.clear();
}
std::string OrderManager::getElementType(){
    return "OrderManager";
}
