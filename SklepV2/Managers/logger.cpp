#include "logger.h"
#include <ctime>
#include <iostream>

std::ofstream Logger::logFile;
std::string Logger::logFileName="loggerOutput.log";

Logger & Logger::getLogger(){
    static Logger logger;
    logger.logFile.open(logFileName, std::ios_base::app);
    return logger;
}

void Logger::Log(const std::string &msg, int type){
    std::string logHead;
    switch(type){
    case 1:
        logHead = "ERROR";
        break;
    case 2:
        logHead = "INFO";
        break;
    case 3:
        logHead = "DEBUG";
        break;
    default:
        logHead = "LOG";
        break;
    }

    // current date/time based on current system
    time_t now = time(0);

    // convert now to string form
    char* dt = ctime(&now);
    logFile<<"["<<logHead<<"]"<<dt<<" : "<<msg<<"\n";
    logFile.close();
}
