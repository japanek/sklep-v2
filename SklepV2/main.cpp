#include <iostream>
#include <tuple>
#include <string>

#include "Managers/ordermanager.h"
#include "Objects/productfactory.h"
#include "Objects/shopposition.h"
#include "Managers/logger.h"

int main()
{
    Stock::addToStock(new ShopPosition(ProductFactory::createProduct("Milk"),5));
    Stock::addToStock(new ShopPosition(ProductFactory::createProduct("Bread"),10));
    Stock::addToStock(new ShopPosition(ProductFactory::createProduct("Sugar"),1));
    OrderManager orderMan;
    orderMan.init(&Basket::getBasket(),&Stock::getStock());
    Stock::readStockStateFromFile("stock.txt");
    return 0;
}
