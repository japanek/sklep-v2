#ifndef PRODUCT_H
#define PRODUCT_H

#include <string>
#include <tgmath.h>

class Product
{
public:
    virtual ~Product(){}
    std::string name;
    float priceBrutto;
    float priceNetto;
    float tax;
    std::string unit;

};

#endif // PRODUCT_H
