#ifndef PROGRAMCLASS_H
#define PROGRAMCLASS_H

#include <string>

class ProgramClass
{
public:
    std::string getElementType();
};

#endif // PROGRAMCLASS_H
