#include "milk.h"

Milk::Milk() {
    name = "Milk";
    priceBrutto = 3.2f;
    tax = 0.22 * priceBrutto;
    tax = round(tax);
    priceNetto = tax + priceBrutto;
    unit = "l";
}
