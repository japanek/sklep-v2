#include "sugar.h"

Sugar::Sugar(){
    name = "Sugar";
    priceBrutto = 2.3f;
    tax = 0.22 * priceBrutto;
    round(tax);
    priceNetto = tax + priceBrutto;
    unit = "kg";
}
