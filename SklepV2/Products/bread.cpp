#include "bread.h"

Bread::Bread() {
    name = "Bread";
    priceBrutto = 2.5f;
    tax = 0.22 * priceBrutto;
    tax = round(tax);
    priceNetto = tax + priceBrutto;
    unit = "unit";
}
