TEMPLATE = app
CONFIG -= core gui
CONFIG += console c++14
CONFIG += -std=c++14
CONFIG -= app_bundle

SOURCES += main.cpp \
    Objects/stock.cpp \
    Objects/basket.cpp \
    Objects/productfactory.cpp \
    Products/milk.cpp \
    Objects/shopposition.cpp \
    Products/bread.cpp \
    Products/sugar.cpp \
    Managers/ordermanager.cpp \
    Managers/logger.cpp \
    Interfaces/programclass.cpp

HEADERS += \
    Interfaces/product.h \
    Objects/stock.h \
    Objects/basket.h \
    Objects/productfactory.h \
    Products/milk.h \
    Objects/shopposition.h \
    Products/bread.h \
    Products/sugar.h \
    Managers/ordermanager.h \
    Managers/logger.h \
    Interfaces/programclass.h

DISTFILES += \
    stock.txt
